import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import Markdown from 'vite-plugin-markdown'
import legacy from '@vitejs/plugin-legacy'
import { visualizer } from 'rollup-plugin-visualizer'
import markdownIt from './src/news/news.markdownit'

export default defineConfig({
  base: process.env.BASE_URL,
  plugins: [
    vue(),
    Markdown({
      mode: 'html',
      markdownIt
    }),
    legacy({
      targets: [ 'defaults', 'last 4 iOS major versions' ]
    }),
    visualizer({ open: true })
  ],
  server: {
    port: 8080
  }
})
