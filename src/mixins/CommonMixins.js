import { getShortLocale } from '../shared'

export default {
  methods: {
    buildImgUrl: function (imageName) {
      return import.meta.env.BASE_URL + 'img/' + imageName
    },

    buildPublicUrl: function (name) {
      return import.meta.env.BASE_URL + name
    },

    buildRoute: function (route) {
      if (this.localePath) return '/' + this.localePath + route

      return route
    },

    getCurrentShortLocale () {
      return getShortLocale(this.$language.current)
    },

    buildArticles (markdownFilesObj) {
      return Object.keys(markdownFilesObj)
        .map(key => markdownFilesObj[key])
        .map(a => {
          return {
            id: a.attributes.id,
            title: a.attributes.title,
            mastodon: a.attributes.mastodon,
            twitter: a.attributes.twitter,
            date: new Date(a.attributes.date),
            html: a.html
          }
        })
        .sort((a, b) => {
          if (a.date < b.date) return 1
          if (a.date > b.date) return -1
          return 0
        })
    }
  }
}
