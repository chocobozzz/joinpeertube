---
id: roadmap-v4
title: Nos projets pour PeerTube v4
date: April 13, 2021
---

<p>Dans <a href="https://framablog.org/2021/01/07/peertube-v3-ca-part-en-live/" target="_blank">l'article blog de la
    sortie de PeerTube v3</a>, nous avons annoncé que nous n'aurions pas recours
  au *crowdfunding* pour financer le développement de PeerTube en 2021.</p>
<p>Nous sommes heureuses d'annoncer qu'une grande partie de PeerTube v4 sera financée par
  une bourse de NLnet dans le cadre de leur programme <a href="https://nlnet.nl/discovery/" target="_blank">"Search
    and discovery" (EN)</a> (voir <a href="https://nlnet.nl/project/PeerTube/" target="_blank">ici</a> leur page sur
  PeerTube (EN)).</p>
<p>Nous prévoyons de développer la v4 de PeerTube tout au long de 2021, en nous
  concentrant principalement sur la personnalisation de l'expérience afin que les administratrices d'instance, les
  créateurs de contenu et les utilisatrices puissent trouver et découvrir les vidéos à leur goût.</p>
<p>Nous sommes convaincus que le fait de permettre aux administrateurs d'instances et aux
  créatrices de contenu de personnaliser leurs instances et leurs chaînes est une excellente façon d'aider les
  utilisateurs de PeerTube à trouver et à identifier les vidéos qu'ils souhaitent regarder.</p>
<p>Cette personnalisation aura un impact direct sur la recherche, et sur les résultats de
  la recherche. De plus, nous prévoyons d'étendre la portée de la recherche aux listes de lecture afin qu'elles
  puissent être affichées dans les résultats de recherche. Nous prévoyons également d'améliorer les pages de résultats
  de recherche avec un système de tri/filtre plus efficace et une meilleure interface.</p>
<h4>Principales fonctionnalités de PeerTube v4
</h4>
<p>Voici les principales fonctionnalités de « personnalisation » sur lesquelles nous
  prévoyons de travailler :</p>
<ul>
  <li>Système de tri/filtre
    sur les pages qui listent les vidéos</li>
  <li>
    Personnalisation du menu de gauche pour les instances (via l'API du système de plugins)</li>
  <li>
    Personnalisation de la page d'accueil des instances (via un système de balisage)</li>
  <li>Personnalisation des chaînes
    pour les vidéastes</li>
  <li>Incitations à compléter les attributs des instances/chaînes (c'est-à-dire les
    descriptions, les images, les conditions de service, etc.)</li>
</ul>
<p>Nous
  prévoyons également de travailler sur des améliorations fréquemment demandées telles que :</p>
<ul>
  <li>Ajout de listes de lecture au champ de recherche (sur l'instance PeerTube et Sepia
    Search)</li>
  <li>Abonnement à des
    chaînes/comptes à partir d'une instance</li>
  <li>Coordination avec support et contributions à des développements externes (clients,
    plugin, etc.)</li>
</ul>
<p>Vous trouverez plus de détails sur nos projets sur la <a target="_blank" rel="noreferrer noopener"
    href="https://github.com/Chocobozzz/PeerTube/projects/6">page Github de PeerTube</a>.
  Veuillez noter qu'il s'agit des principales fonctionnalités et améliorations. Elles peuvent évoluer en fonction des
  circonstances, des contributions, des délais, etc. Pour être informé·e de tout changement, <a target="_blank"
    rel="noreferrer noopener" href="https://framalistes.org/sympa/subscribe/peertube-newsletter">inscrivez-vous à la
    newsletter PeerTube</a>.</p>
<h4>Tempérer les attentes</h4>
<p>
  C'est déjà beaucoup, et nous sommes impatient·es de mettre en œuvre ces fonctionnalités !</p>
  <p>Ceci étant dit, nous comprenons que beaucoup de gens ont beaucoup d'attentes envers
    PeerTube. Mais nous n'allons pas toutes les remplir et tout développer, car nous ne sommes pas un géant de la
    technologie, ni même une startup.</p>
  <p><a target="_blank" rel="noreferrer noopener" href="https://framasoft.org/fr">Framasoft</a> est une association à but
    non lucratif de 35 membres, qui a choisi
    de ne pas trop grandir. Nous voulons rester à 10 employés au maximum, même si nous gérons plus de 80 projets
    différents (d'accord, PeerTube est l'un des plus gros, mais c'est quand même l'un des quatre-vingts projets).</p>
  <p>Nous n'avons qu'un seul développeur dédié à PeerTube (même pas à plein temps), aidé
    par d'autres membres de notre équipe (conception, administration, communication, etc.). La maintenance de PeerTube
    implique de nombreuses tâches différentes (en savoir plus <a target="_blank" rel="noreferrer noopener"
      href="https://joinpeertube.org/faq#who-does-what-in-peertube">ici dans notre FAQ</a>), et nous ne voulons pas
    qu'il (ou quiconque, d'ailleurs) s'épuise à répondre aux attentes de toutes et de tous.</p>
  <p>Donc, si nos plans pour 2021 et la v4 de PeerTube ne correspondent pas à vos attentes,
    c'est tout à fait OK, mais nous ne ferons pas mieux, ni plus vite. Rappelez-vous que vous êtes libre de forker
    PeerTube pour le mener dans une autre direction, ou de contribuer en codant des plugins et des issues (<a
      target="_blank" rel="noreferrer noopener" href="https://docs.joinpeertube.org/contribute-getting-started">commencez
      à contribuer en regardant par ici</a>).
</p>
<h4>Merci pour le soutien</h4>
<p>Nous tenons à remercier la communauté PeerTube pour son travail et son soutien. Nous
  tenons également à remercier NLnet pour son don qui financera la majeure partie de notre travail sur PeerTube cette
  année, nous évitant ainsi d'avoir à organiser un crowdfunding spécifique en 2021.</p>
<p>Si la majeure partie de notre feuille de route pour PeerTube est couverte par la
  bourse de NLnet, le reste proviendra de notre propre budget. Framasoft est financée par les personnes qui nous
  donnent de l'argent pour l'ensemble de nos actions et projets.</p>
<p>Si vous voulez soutenir PeerTube, vous pouvez <a target="_blank" rel="noreferrer noopener"
    href="https://soutenir.framasoft.org/fr">soutenir Framasoft par un don</a>, mais aussi
  en aidant d'autres personnes à découvrir et <a target="_blank" rel="noreferrer noopener"
    href="https://joinpeertube.org/">à en savoir plus sur PeerTube</a> et sur l'ensemble de nos projets : partager,
  c'est déjà contribuer !</p>
