---
id: roadmap-v3-part-2
title: "[V3 Roadmap] Moderation ✅, let's improve plugins and playlists!"
date: August 17, 2020
mastodon: https://framapiaf.org/@Framasoft/104705579480495710
twitter: https://twitter.com/framasoft/status/1295390412744663041
---

In early August, we entered a new stage in [the development of PeerTube v3](https://joinpeertube.org/roadmap/).

<h4 id="roadmap-v3-part-2-moderation-tools">Moderation tools: on the road to 2.4</h4>

In early August we finalized the work on the moderation tools: accounts and comments reporting, improving the administration and moderation interface, reporting logs, messages between the moderation team and the reporter…

We will talk more about these tools when we will announce the 2.4 update which is currently in the testing and bugfixing phase.

There is also a new plugin that allows to silence or block a list of predefined instances: our thanks to those who have started to create, publish and share their blocking lists, we hope that this kind of initiatives will multiply!

<img loading="lazy" src="/img/news/roadmap-v3-part-2/en/2020-05-21_Peertube-Moderation_by-David-Revoy lowres.jpg" alt="">

<h4 id="roadmap-v3-part-2-playlists-plugins">Playlists, plugins…</h4>

As planned, during August and September, we will work to improve the playlists and plugins systems. First, we will work on the embedding of playlists so that they can be shared online.

PeerTube's playlists already allow you to list just one clip of a video (and not the whole video), which can be very useful (especially for educational purposes), but they do not yet allow you to include several clips of one video in the same playlist, so we want to remedy this.

We've already developed several plugins in the last few weeks, which in many cases increases the possibilities of interaction between the plugins and the core PeerTube software. We're going to continue along this path, by creating an annotation plugin, which allows you to display information at a given point in a video.

By the way, if you have created a plugin for PeerTube, feel free to tell us about it [on our contributions forum](https://framacolibri.org/c/peertube): we are in the process of making a selection to highlight it on JoinPeertube.org.

<img loading="lazy" src="/img/news/roadmap-v3-part-2/en/2020-05-21_Peertube-Plugin_by-David-Revoy lowres.jpg" alt="">

<h4 id="roadmap-v3-part-2-more">The next two months will be even busier!</h4>

The work in progress on the PeerTube experience is going well: several interviews have been conducted, and our designer will soon be sending us summaries with her recommendations. In addition, we are planning to work together on the menus organization, but don't be impatient! This is an in-depth work and the results may only be visible in a few months 😉.

Finally, we had many feedbacks following the release of version 2.3 presenting the global search features. We feel that an important tool is missing: an independent website for the search engine that indexes PeerTube videos.

So we plan to take some time from this third stage of development to create such a web interface and put it online. Of course, this code will be free so that others can publish their PeerTube search page with their own choice of instances to index.

<img loading="lazy" src="/img/news/roadmap-v3-part-2/en/2020-05-21_Peertube-Research_by-David-Revoy lowres.jpg" alt="">

<h4 id="roadmap-v3-part-2-share">We need your help: please share!</h4>

After a dazzling start (thanks to you all), the progressive fundraising for this roadmap has slowed down a lot. Currently, we are halfway through the €60,000 needed to finance these six months of development. Concretely, this means that from the beginning of September, we will finance the progress towards v3 on our own 2020 budget.

So we need your help to share the roadmap around you, and get the word out about PeerTube. Feel free to share this project in your blogs, vlogs, podcasts: some of you have already done so, and it helps us a lot (plus it really warms our free-software-lovers' hearts)!

We count on you to share the roadmap: https://joinpeertube.org/roadmap
