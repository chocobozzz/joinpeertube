---
id: plugins-selection-1
title: A plugin selection on joinpeertube.org
date: March 11, 2021
---

<p>Hello,</p>

<p>
  In September 2019 when <a rel="noreferrer noopener" target="_blank"
    href="https://joinpeertube.org/fr/news#release-1-4-0">PeerTube v.1.4 was released</a>, we announced the creation of
  a plugin system. This system allows PeerTube instance administrators to create and/or install plugins depending on
  their specific feature needs, without having to rely on our small non-profit for this creative work.
</p>

<p>Did you know that there are now more than 40 plugins available for PeerTube instance administrators?</p>

<p>
  To support the installation of these plugins, we have just created <a rel="noreferrer noopener" target="_blank"
    href="https://joinpeertube.org/plugins-selection">a new page</a> in which we will often highlight a plugin
  selection. Thus we hope that more and more instance administrators will install them to improve their instance
  features.
</p>

<p>We first have chosen to highlight 4 plugins:</p>

<ul>
  <li>
    <a rel="noreferrer noopener" target="_blank" href="/plugins-selection#video-annotation">video-annotation</a> to add
    annotations to a video;
  </li>

  <li>
    <a rel="noreferrer noopener" target="_blank" href="/plugins-selection#upload-limits">upload-limits</a> to alert on
    the upload-limits;
  </li>

  <li>
    <a rel="noreferrer noopener" target="_blank" href="/plugins-selection#chapters">chapters</a> to add chapters to a
    video;
  </li>

  <li>
    <a rel="noreferrer noopener" target="_blank" href="/plugins-selection#glavliiit">glavliiit</a> to help with the
    moderation.
  </li>
</ul>

<p>
  We will regularly add other plugins to this selection. Have a good discovery!
</p>
