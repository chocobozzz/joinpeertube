---
id: joinpeertube-2023
title: JoinPeertube gets a redesign, PeerTube v5 on its way!
date: November 29, 2022
---

Hello,

First of all, PeerTube v5 is on its way, as we have just published a release candidate version that will be tested out in the next few weeks. We can't wait to present you the improvements and new features!

In the meantime, we have just published a brand new redesign of [JoinPeerTube](https://joinpeertube.org). It took a lot of work to simplify this website. Our intention is to make it a welcoming tool, a kind of gateway into the PeerTube universe for everyone, especially for non-tech savvy people.

* Get [a tour of this new JoinPeerTube](https://framablog.org/?p=29181) (backstage infos included) on our blog;
* Visit [the brand new JoinPeertube.org](https://joinpeertube.org) (and share it around you!);
* Support PeerTube by [supporting Framasoft](https://soutenir.framasoft.org/en/) (that's us!), and all our actions.

![screenshot of the header of the new joinpeertube.org](https://framablog.org/wp-content/uploads/2022/11/2022-11-28-16-36-chocobozzz.frama.io.png)

We really hope this new website will help content creators and content enjoyers get a better grip on what PeerTube is and is not, and how to get started.

We, at Framasoft, are a small French non-profit that is exclusively funded by donations (87% of our budget being grassroots donations). We don't really know how to PR and market, especially in English. So most of our funding (that finances PeerTube's development, among other things) comes from the French-speaking audience.

Today, we have reached 27% of our 2022 donation campaign goals. We need you to help us spread the word in the non-French-speaking community that all our projects need support! If you want to help us fund PeerTube and many other projects in 2023, please consider [supporting Framasoft](https://soutenir.framasoft.org/en/).

We hope you will enjoy and share this new [Joinpeertube.org](https://joinpeertube.org),
Framasoft
