---
id: release-4.2
title: PeerTube v4.2 is out!
date: June 7, 2022
---

Hi everybody,

We are pleased to announce this new version of PeerTube!

Editing videos from the web interface, detailed viewers stats for videos, ability to adjust latency during a live broadcast and much more...  Let's look around and see what it brings us!

### Editing videos from PeerTube web interface

This new feature will delight upstarting videomakers! Until now, there was no options for video editing on PeerTube: videomakers had to edit their videos before publishing them. Now, if you need to do some basic editing, you can do it directly in the web interface.

Once you have uploaded your original video, simply click on the `...` menu and select *Studio*. You can then choose to :
  * cut the video (by specifying a new start and/or new end time-code)
  * add an intro at the beginning and/or an outro at the end of the video (by uploading a video file)
  * add a watermark to the top right corner of the video (by uploading an image)

Once you made these changes, PeerTube will automatically transcode the new video and replace the original one.

![](/img/news/release-4.2/en/EN-studio.jpg)

This feature has been developed with the financial support of the "Direction du numérique pour l'Éducation du  Ministère de l'Éducation Nationale, de la Jeunesse et des Sports" (french Ministry of National Education). It was a request from the team in charge of the [apps.education.fr platform](https://apps.education.fr/), which offers remote working tools to French employees and teachers. Thanks to them!

### Stats on videos

Until now, PeerTube offered very few metrics for videos: just a view and a like counter. This 4.2 version provides new metrics for each video:
  * average and total watch time
  * peak viewers
  * number of viewers' origin countries

To access the stats page, click on `...` button located under the video player (on the right) and select *Stats*.

![](/img/news/release-4.2/en/EN-stats.jpg)

In addition to these counters, PeerTube allows you to view the following data through interactive graphs:
  * total viewers
  * aggregated watch time
  * viewers by country (if the instance's admin have not disabled this option)
  * audience retention (to identify the moments in your videos that captured the audience's attention)

This feature has been developed with the financial support of HowlRound Theatre Commons at Emerson College. Thanks to them!

### Save replay of permanent/recurring live streaming sessions

As you probably already know, PeerTube offers 2 ways to broadcast a live:
  * "normal" live: where you can stream only once
  * permanent/recurring live: where you can stream multiples times using the same URL

Until now, only the "normal" live allowed to publish a replay of the livestream. When using permanent/recurring live, it was not possible to automatically record replays. You had to use an external tool to record a streaming session and publish it manually in PeerTube, just like any other video.

Now, it is possible to save each permanent/recurring live streaming session as a replay on a new url. Convenient, isn't it? Replays are automatically filled with all permanent/recurring live information for a better indexation.

![](/img/news/release-4.2/en/EN-rediff-automatique-live.jpg)

This feature has also been developed with the financial support of HowlRound Theatre Commons at Emerson College. Thanks to them!

### Latency settings support for lives

Currently, when broadcasting live on PeerTube, the latency (time between when the video stream is sent and when the video is watched) is estimated to be around 30-40 seconds. This is because PeerTube uses a peer-to-peer protocol (P2P) to broadcast videos, lowering the load of their hosts.

In order to change this latency, we offer videomakers two new settings:
  * reduced latency, by turning off P2P
  * increased latency, to exchange more efficiently video segments with P2P

To change this setting, simply edit the live settings. In the *Live settings* tab, you can select the latency mode you want.

![](/img/news/release-4.2/en/EN-latency-mode.jpg)

### And also

Thanks to an external contribution, it is now very convenient to **edit video subtitles directly from the web interface**.

PeerTube also allows instances' administrators to **display author avatar in video miniatures** on the various pages that list videos (*Local Videos* for example).

We have made many other improvements in this new version. You can read the whole list on [https://github.com/Chocobozzz/PeerTube/blob/develop/CHANGELOG.md](https://github.com/Chocobozzz/PeerTube/blob/develop/CHANGELOG.md).

Thanks to all PeerTube contributors!
Framasoft
