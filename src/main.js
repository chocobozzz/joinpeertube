import { createApp } from 'vue'
import VueMatomo from 'vue-matomo'
import { createRouter, createWebHistory } from 'vue-router'
import { createGettext } from 'vue3-gettext'
import { createHead, VueHeadMixin } from '@vueuse/head'
import VueProgressBar from '@aacassandra/vue3-progressbar'

import App from './App.vue'
import Home from './views/Home.vue'
import NotFound from './views/NotFound.vue'
import PublishVideos from './views/PublishVideos.vue'
import Contribute from './views/Contribute.vue'

import './scss/main.scss'

import SafeHTML from './components/SafeHTML.vue'
import CommonMixins from './mixins/CommonMixins'

const app = createApp(App)

// ############# I18N ##############

const availableLanguages = {
  'en_US': 'English',
  'fr_FR': 'Français',
  'ar': 'العربية',
  'de': 'Deutsch',
  'es': 'Español',
  'eo': 'Esperanto',
  'gd': 'Gàidhlig',
  'it': 'Italiano',
  'hr': 'hrvatski',
  'pl': 'Polski',
  'pt_BR': 'Português',
  'ru': 'русский',
  'sq': 'Shqip',
  'sv': 'svenska',
  'hu': 'magyar',
  'gl': 'galego',
  'ja': '日本語',
  'tr': 'Türkçe',
  'zh_Hans': '简体中文（中国）',
  'zh_Hant': '繁體中文（台灣）'
}
const aliasesLanguages = {
  'en': 'en_US',
  'fr': 'fr_FR',
  'pt': 'pt_BR'
}
const allLocales = Object.keys(availableLanguages).concat(Object.keys(aliasesLanguages))

const defaultLanguage = 'en_US'
let currentLanguage = defaultLanguage

const basePath = import.meta.env.BASE_URL
const startRegexp = new RegExp('^' + basePath)

const paths = window.location.pathname
  .replace(startRegexp, '')
  .split('/')

const localePath = paths.length !== 0 ? paths[0] : ''
let languageFromLocalStorage

try {
  languageFromLocalStorage = localStorage.getItem('language')
} catch (err) {
  console.error('Cannot fetch current language from local storage.', err)
}

app.config.globalProperties.localePath = ''

if (allLocales.includes(localePath)) {
  currentLanguage = aliasesLanguages[localePath] ? aliasesLanguages[localePath] : localePath

  app.config.globalProperties.localePath = localePath

  try {
    localStorage.setItem('language', currentLanguage)
  } catch (err) {
    console.error('Cannot set language in local storage.', err)
  }
} else if (languageFromLocalStorage) {
  currentLanguage = languageFromLocalStorage
} else {
  const navigatorLanguage = window.navigator.userLanguage || window.navigator.language
  const snakeCaseLanguage = navigatorLanguage.replace('-', '_')
  currentLanguage = aliasesLanguages[snakeCaseLanguage] ? aliasesLanguages[snakeCaseLanguage] : snakeCaseLanguage
}

const p = buildTranslationsPromise(defaultLanguage, currentLanguage)

p.catch(err => {
  console.error('Cannot load translations.', err)
  return { default: {} }
}).then(translations => {
  const gettext = createGettext({
    translations,
    availableLanguages,
    defaultLanguage: currentLanguage,
    mutedLanguages: [ 'en_US' ]
  })

  app.use(gettext)

  // ###########################

  app.component('SafeHTML', SafeHTML)

  app.mixin(CommonMixins)

  const News = () => import('./views/News.vue')
  const NewsArchive = () => import('./views/NewsArchive.vue')
  const AllPluginsSelection = () => import('./views/plugin-selection/AllPluginsSelection.vue')
  const FAQ = () => import('./views/FAQ.vue')
  const BrowseContent = () => import('./views/BrowseContent.vue')
  const Instances = () => import('./views/Instances.vue')

  let routes = []

  // eslint-disable-next-line no-unused-vars
  const sitemapUrl = []

  for (const locale of [ '' ].concat(allLocales)) {
    const base = locale
      ? '/' + locale + '/'
      : '/'

    routes = routes.concat([
      {
        path: '/' + locale,
        component: Home
      },
      {
        path: base + 'help',
        redirect: '/' + locale
      },
      {
        path: base + 'news',
        component: News
      },
      {
        path: base + 'news-archive',
        component: NewsArchive
      },
      {
        path: base + 'instances',
        component: Instances
      },
      {
        path: base + 'faq',
        component: FAQ
      },
      {
        path: base + 'contents-selection',
        redirect: () => {
          return { path: '/' + locale, hash: '#find-peertube-instances' }
        }
      },
      {
        path: base + 'plugins-selection',
        component: AllPluginsSelection
      },
      {
        path: base + 'publish-videos',
        component: PublishVideos
      },
      {
        path: base + 'contribute',
        component: Contribute
      },
      {
        path: base + 'browse-content',
        component: BrowseContent
      }
    ])
  }

  // If we need to update sitemap
  // displaySitemapInConsole(allLocales)

  routes.push({
    path: '/404',
    component: NotFound
  })

  routes.push({
    path: '/*',
    redirect: '/404'
  })

  let scrollBehavior
  if (
    window.location.host !== 'webcache.googleusercontent.com' && // Don't break google cache
    /bot|googlebot|crawler|spider|robot|crawling/i.test(navigator.userAgent) === false
  ) {
    scrollBehavior = (to, _from, savedPosition) => {
      if (savedPosition) return savedPosition
      if (to.hash) return { el: to.hash }

      // Have to use a promise here
      // FIXME: https://github.com/vuejs/router/issues/1411
      return new Promise(res => setTimeout(() => {
        res({ top: 0, left: 0 })
      }))
    }
  } else {
    console.log('Disabling scroll behaviour.')
  }

  const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes,
    scrollBehavior
  })

  // Stats Matomo
  if (!(navigator.doNotTrack === 'yes' || navigator.doNotTrack === '1')) {
    app.use(VueMatomo, {
      // Configure your matomo server and site
      host: 'https://stats.framasoft.org/',
      siteId: 68,

      // Enables automatically registering pageviews on the router
      router,

      // Require consent before sending tracking information to matomo
      // Default: false
      requireConsent: false,

      // Whether to track the initial page view
      // Default: true
      trackInitialView: true,

      // Changes the default .js and .php endpoint's filename
      // Default: 'piwik'
      trackerFileName: 'p',

      enableLinkTracking: true
    })
  }

  app.use(router)

  app.mixin(VueHeadMixin)
  app.use(createHead())

  const progressBarOptions = {
    color: '#F2690D'
  }
  app.use(VueProgressBar, progressBarOptions)

  app.mount('#app')
}).catch(err => console.error(err))

function buildTranslationsPromise (defaultLanguage, currentLanguage) {
  const translations = {}
  const oldTranslationKeys = [ 'translations-v1-', 'translations-v2-', 'translations-v3-' ]
  const translationKey = 'translations-v4-'

  // No need to translate anything
  if (currentLanguage === defaultLanguage) return Promise.resolve(translations)

  // Cleanup
  try {
    for (const key of oldTranslationKeys) {
      localStorage.removeItem(key + currentLanguage)
    }
  } catch (err) {
    console.error('Cannot remove old translations keys')
  }

  // Fetch translations from server
  const fromRemote = import(`./translations/${currentLanguage}.json`)
    .then(module => {
      const remoteTranslations = module.default
      try {
        localStorage.setItem(translationKey + currentLanguage, JSON.stringify(remoteTranslations))
      } catch (err) {
        console.error('Cannot save translations in local storage.', err)
      }

      return Object.assign(translations, remoteTranslations)
    })

  // If we have a cache, try to
  try {
    const fromLocalStorage = localStorage.getItem(translationKey + currentLanguage)

    if (fromLocalStorage) {
      Object.assign(translations, JSON.parse(fromLocalStorage))

      return Promise.resolve(translations)
    }
  } catch (err) {
    console.error('Cannot parse translations from local storage.', err)
  }

  return fromRemote
}

// eslint-disable-next-line no-unused-vars
function displaySitemapInConsole (allLocales) {
  const filteredLocales = allLocales.filter(l => l !== 'en' && l !== 'en_US')

  const paths = [
    '',
    '/help',
    '/news',
    '/news-archive',
    '/instances',
    '/faq',
    '/contents-selection',
    '/plugins-selection',
    '/publish-videos',
    '/contribute',
    '/browse-content',
  ]

  const indentation = '    '
  let message = ''

  message += '<?xml version="1.0" encoding="UTF-8"?>\n'
  message += '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"\n'
  message += indentation + indentation + 'xmlns:xhtml="http://www.w3.org/1999/xhtml">\n\n'

  for (const path of paths) {
    message += indentation + '<url>\n'
    message += indentation + indentation + '<loc>https://joinpeertube.org/' + path + '</loc>\n'
    message += indentation + indentation + '<xhtml:link rel="alternate" hreflang="en" href="https://joinpeertube.org/' + path + '" />\n'

    for (const locale of filteredLocales) {
      message += indentation + indentation + '<xhtml:link rel="alternate" hreflang="' + locale + '" href="https://joinpeertube.org/' + locale + path + '" />\n'
    }

    message += indentation + '</url>\n\n'
  }

  message += '</urlset>\n'

  console.log(message)
}
