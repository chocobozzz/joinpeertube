function buildHeadTitle (titleArg) {
  const title = titleArg + ' | JoinPeerTube'

  return {
    title,

    meta: [
      {
        property: 'og:title',
        content: title
      },
      {
        name: 'twitter:title',
        content: title
      }
    ]
  }
}

function buildHeadDescription (description) {
  return {
    meta: [
      {
        name: 'description',
        content: description
      },
      {
        property: 'og:description',
        content: description
      },
      {
        name: 'twitter:description',
        content: description
      }
    ]
  }
}

export {
  buildHeadTitle,
  buildHeadDescription
}
