# JoinPeerTube

## Install dependencies

Install the package manager

```
npm install --global yarn
```

Fetch the dependencies

```
$ yarn install --pure-lockfile
```

## Dev

```
$ npm run dev
```

## Build for production

```
$ npm run build
```

## Update translations

Install easygettext

```
$ npm install -g easygettext
```

Add Weblate remote:

```
$ git remote add weblate https://weblate.framasoft.org/git/joinpeertube/main
```

Update from Weblate:

```
$ git fetch weblate && git merge weblate/master
```

Re generate translations:

```
$ npm run i18n:update
```

Push on master (Weblate will be automatically updated)

```
$ git push origin master
```

## News

To add a news, add markdown files in `src/news/en` and `src/news/fr` and rebuild `npm run build`.
To archive a news, move it in `src/news/archives` and rebuild `npm run build`.


## Add locale

Add the locale in `src/main.js` and `Makefile`. Then update translations.
